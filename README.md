# Overview

Name                                         | About                                                                                                                                                        | Whitepaper                                                                
---------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------
[Bitcoin](https://bitcoin.org/en)            |                                                                                                                                                              | [Bitcoin whitepaper](docs/whitepapers/Bitcoin/bitcoin.pdf)
[BitConnect](https://bitconnectcoin.co/)     | uses scrypt; uses PoS;                                                                                                                                       | 
[BitShares](https://bitshares.org/)          | Platform that provides: Price-Stable Cryptocurrencies, Decentralized Asset Exchange, Industrial Performance and Scalability, ...                             | 
[Dash](https://www.dash.org/)                | Allows instant transaction confirmation withour a centralized authority, improves privacy using decentralized mixing service; uses Masternode network        | [Dash Whitepaper](docs/whitepapers/Dash/DashPaper-v13-v1.pdf)            
[EOS](https://eos.io/)                       | Ethereum based                                                                                                                                               | [EOS whitepaper](docs/whitepapers/EOS/TechnicalWhitePaper.md)            
[Ethereum](https://ethereum.org/)            |                                                                                                                                                              | [Ethereum whitepaper](https://github.com/ethereum/wiki/wiki/White-Paper)
[Litecoin](https://litecoin.org/)            | Like Bitcoin, but uses scrypt instead of sha256, and transactions are 4 times faster                                                                         |
[Monero](https://getmonero.org/)             | Hides sender, amount, receiver in transaction; ASIC-resistant proof-of-work algorithm;                                                                       | [Monero whitepaper](docs/whitepapers/Monero/whitepaper.pdf)
[NEM](https://www.nem.io/)                   | NEM is a solution platform, that allow you to send and receive payments and messages quickly, securely and at low cost on a global scale                     | [NEM whitepaper](docs/whitepapers/NEM/NEM_techRef.pdf)                   
[Stratis](https://stratisplatform.com/)      | Platform for creation private blockchains from scratch                                                                                                       | [Stratis whitepaper](docs/whitepapers/Stratis/Stratis_Whitepaper.pdf)
[TenX](https://www.tenx.tech/)               | Enables users to spend their blockchain assets through their smartphone or a physical debit card at over 36 million points of acceptance online and  offline | [TenX whitepaper](docs/whitepapers/TenX/tenx_whitepaper_final.pdf)       
[Tether](https://tether.to/)                 | Tether: Fiat currencies on the Bitcoin blockchain                                                                                                            | [Tether whitepaper](docs/whitepapers/Tether/TetherWhitePaper.pdf)        
[Zcash](https://z.cash/)                     | Currency with strong privacy guarantees                                                                                                                      | [Zcash whitepaper](docs/whitepapers/Zcash/zerocash-extended-20140518.pdf)


[Ripple](https://ripple.com/)                |
[IOTA](http://iota.org/)                     |
[OmiseGo](https://omg.omise.co/)             |
[Qtum](http://qtum.org/)                     |
[NEO](https://neo.org/)                      | 
[Waves](https://wavesplatform.com/)          |          


[Bitcoin Cash](https://www.bitcoincash.org/)           |
[Etherium Classic](https://ethereumclassic.github.io/) | 



# References

[COMIT white paper](http://www.comit.network/doc/COMIT%20white%20paper%20v1.0.2.pdf)

# Questions to answer

1. Proof of reserves
2. Proof of stake
3. COMIT network
4. Proof of importance
5. Proof of service
